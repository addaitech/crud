<?php

class AccountsController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('accounts');
        $this->load->library('session');
    }
    /*
    function for manage Accounts.
    return all Accountss.
    created by your name
    created at 04-04-18.
    */
    public function manageAccounts() {
        $data["accountss"] = $this->accounts->getAll();
        $this->load->view('accounts/manage-accounts', $data);
    }
    /*
    function for  add Accounts get
    created by your name
    created at 04-04-18.
    */
    public function addAccounts() {
        $this->load->view('accounts/add-accounts');
    }
    /*
    function for add Accounts post
    created by your name
    created at 04-04-18.
    */
    public function addAccountsPost() {
        $data['account'] = $this->input->post('account');
        $data['type'] = $this->input->post('type');
        $data['salutation'] = $this->input->post('salutation');
        $data['firstname'] = $this->input->post('firstname');
        $data['lastname'] = $this->input->post('lastname');
        $data['title'] = $this->input->post('title');
        $data['title'] = $this->input->post('title');
        $data['email'] = $this->input->post('email');
        $data['address1'] = $this->input->post('address1');
        $data['address2'] = $this->input->post('address2');
        $data['city'] = $this->input->post('city');
        $data['state'] = $this->input->post('state');
        $data['zip'] = $this->input->post('zip');
        $data['homephone'] = $this->input->post('homephone');
        $data['cellphone'] = $this->input->post('cellphone');
        $data['workphone'] = $this->input->post('workphone');
        $data['created'] = $this->input->post('created');
        $this->accounts->insert($data);
        $this->session->set_flashdata('success', 'Accounts added Successfully');
        redirect('manage-accounts');
    }
    /*
    function for edit Accounts get
    returns  Accounts by id.
    created by your name
    created at 04-04-18.
    */
    public function editAccounts($accounts_id) {
        $data['accounts_id'] = $accounts_id;
        $data['accounts'] = $this->accounts->getDataById($accounts_id);
        $this->load->view('accounts/edit-accounts', $data);
    }
    /*
    function for edit Accounts post
    created by your name
    created at 04-04-18.
    */
    public function editAccountsPost() {
        $accounts_id = $this->input->post('accounts_id');
        $accounts = $this->accounts->getDataById($accounts_id);
        $data['account'] = $this->input->post('account');
        $data['type'] = $this->input->post('type');
        $data['salutation'] = $this->input->post('salutation');
        $data['firstname'] = $this->input->post('firstname');
        $data['lastname'] = $this->input->post('lastname');
        $data['title'] = $this->input->post('title');
        $data['title'] = $this->input->post('title');
        $data['email'] = $this->input->post('email');
        $data['address1'] = $this->input->post('address1');
        $data['address2'] = $this->input->post('address2');
        $data['city'] = $this->input->post('city');
        $data['state'] = $this->input->post('state');
        $data['zip'] = $this->input->post('zip');
        $data['homephone'] = $this->input->post('homephone');
        $data['cellphone'] = $this->input->post('cellphone');
        $data['workphone'] = $this->input->post('workphone');
        $data['created'] = $this->input->post('created');
        $edit = $this->accounts->update($accounts_id,$data);
        if ($edit) {
            $this->session->set_flashdata('success', 'Accounts Updated');
            redirect('manage-accounts');
        }
    }
    /*
    function for view Accounts get
    created by your name
    created at 04-04-18.
    */
    public function viewAccounts($accounts_id) {
        $data['accounts_id'] = $accounts_id;
        $data['accounts'] = $this->accounts->getDataById($accounts_id);
        $this->load->view('accounts/view-accounts', $data);
    }
    /*
    function for delete Accounts    created by your name
    created at 04-04-18.
    */
    public function deleteAccounts($accounts_id) {
        $delete = $this->accounts->delete($accounts_id);
        $this->session->set_flashdata('success', 'accounts deleted');
        redirect('manage-accounts');
    }
    /*
    function for activation and deactivation of Accounts.
    created by your name
    created at 04-04-18.
    */
    public function changeStatusAccounts($accounts_id) {
        $edit = $this->accounts->changeStatus($accounts_id);
        $this->session->set_flashdata('success', 'accounts '.$edit.' Successfully');
        redirect('manage-accounts');
    }

}