// routes for accounts.
$route['manage-accounts']="AccountsController/ManageAccounts";
$route['change-status-accounts/(:num)']="AccountsController/changeStatusAccounts/$1";
$route['edit-accounts/(:num)']="AccountsController/editAccounts/$1";
$route['edit-accounts-post']="AccountsController/editAccountsPost";
$route['delete-accounts/(:num)']="AccountsController/deleteAccounts/$1";
$route['add-accounts']="AccountsController/addAccounts";
$route['add-accounts-post']="AccountsController/addAccountsPost";
$route['view-accounts/(:num)']="AccountsController/viewAccounts/$1";
