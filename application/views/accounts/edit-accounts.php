<!DOCTYPE html>
<html lang="en">
<head>
    <title>Codeigniter Crud By Crud Generator</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://crudegenerator.in">Codeigniter Crud By Crud Generator</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="<?php echo site_url(); ?>manage-accounts">Manage Accounts</a></li>
            <li><a href="<?php echo site_url(); ?>add-accounts">Add Accounts</a></li>
        </ul>
    </div>
</nav>

<div class="container">

    <h2>Update Accounts</h2>
    <form role="form" method="post" action="<?php echo site_url()?>edit-accounts-post" enctype="multipart/form-data">

        <input type="hidden" value="<?php echo $accounts[0]->id ?>"   name="accounts_id">


        <div class="form-group">
            <label for="account">Account:</label>
            <input type="text" value="<?php echo $accounts[0]->account ?>" class="form-control" id="account" name="account">
        </div>
        <div class="form-group">
            <label for="type">Type:</label>
            <input type="text" value="<?php echo $accounts[0]->type ?>" class="form-control" id="type" name="type">
        </div>
        <div class="form-group">
            <label for="salutation">Salutation:</label>
            <input type="text" value="<?php echo $accounts[0]->salutation ?>" class="form-control" id="salutation" name="salutation">
        </div>
        <div class="form-group">
            <label for="firstname">Firstname:</label>
            <input type="text" value="<?php echo $accounts[0]->firstname ?>" class="form-control" id="firstname" name="firstname">
        </div>
        <div class="form-group">
            <label for="lastname">Lastname:</label>
            <input type="text" value="<?php echo $accounts[0]->lastname ?>" class="form-control" id="lastname" name="lastname">
        </div>
        <div class="form-group">
            <label for="title">Title:</label>
            <select class="form-control" id="title" name="title">
                <option value="Mr" <?php if($accounts[0]->title == "Mr"){ echo "selected"; } ?> >Mr</option>
                <option value=" Ms" <?php if($accounts[0]->title == " Ms"){ echo "selected"; } ?> > Ms</option>
                <option value=" Mrs" <?php if($accounts[0]->title == " Mrs"){ echo "selected"; } ?> > Mrs</option>
            </select>
        </div>
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" value="<?php echo $accounts[0]->title ?>" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" value="<?php echo $accounts[0]->email ?>" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <label for="address1">Address1:</label>
            <input type="text" value="<?php echo $accounts[0]->address1 ?>" class="form-control" id="address1" name="address1">
        </div>
        <div class="form-group">
            <label for="address2">Address2:</label>
            <input type="text" value="<?php echo $accounts[0]->address2 ?>" class="form-control" id="address2" name="address2">
        </div>
        <div class="form-group">
            <label for="city">City:</label>
            <input type="text" value="<?php echo $accounts[0]->city ?>" class="form-control" id="city" name="city">
        </div>
        <div class="form-group">
            <label for="state">State:</label>
            <input type="text" value="<?php echo $accounts[0]->state ?>" class="form-control" id="state" name="state">
        </div>
        <div class="form-group">
            <label for="zip">Zip:</label>
            <input type="text" value="<?php echo $accounts[0]->zip ?>" class="form-control" id="zip" name="zip">
        </div>
        <div class="form-group">
            <label for="homephone">Homephone:</label>
            <input type="text" value="<?php echo $accounts[0]->homephone ?>" class="form-control" id="homephone" name="homephone">
        </div>
        <div class="form-group">
            <label for="cellphone">Cellphone:</label>
            <input type="text" value="<?php echo $accounts[0]->cellphone ?>" class="form-control" id="cellphone" name="cellphone">
        </div>
        <div class="form-group">
            <label for="workphone">Workphone:</label>
            <input type="text" value="<?php echo $accounts[0]->workphone ?>" class="form-control" id="workphone" name="workphone">
        </div>
        <div class="form-group">
            <label for="created">Created:</label>
            <input type="text" value="<?php echo $accounts[0]->created ?>" class="form-control" id="created" name="created">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>